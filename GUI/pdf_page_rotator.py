import easygui as gui
from PyPDF2 import PdfReader, PdfWriter

# 1. Display a file selection dialog for opening a PDF file

input_path = gui.fileopenbox(title="Select a PDF to rotate...", default="*.pdf")

# 2. If the user cancels the dialog, then exit the program

if input_path is None:
    exit()

# 3. Let the user select on of 90, 80 or 270 degrees to rotate the PDF pages

choices = ("90", "180", "270")
message = "Rotate the PDF clockwice by how many degrees?"
degrees = None

while degrees is None:
    degrees = gui.buttonbox(message, "Choose rotation...", choices)
    
# to rotate the pages in the PDF by the selected angle, we’ll need
# the value to be an integer, not a string. Convert it to an integer:
degrees = int(degrees)
# 4. Le the user select one of 90, 180 or 270 degrees to rotate the PDF pages

save_title = "Save the rotated PDF as..."
file_type = "*.pdf"
output_path = gui.filesavebox(title=save_title, default=file_type)

# 5. If the user tries to save with the same name as the input file:

while input_path == output_path:
    # - Alert the user with a message box that this is not allowed
    gui.msgbox(msg="Cannot overwrite original file")
    # - Return to spet four
    output_path = gui.filesavebox(title=save_title, default=file_type)

# 6. If the user cancels the file save dialog, then exit the program/

if output_path is None:
    exit()

# 7. Perform the page rotation:
# - Open the selected PDF

input_file = PdfReader(input_path)
output_pdf = PdfWriter()

# - Rotate all of the pages

for page in input_file.pages:
    page = page.rotate(degrees)
    output_pdf.add_page(page)

# - Save the rotated PDF to the selected file

with open(output_path, "wb") as output_file:
    output_pdf.write(output_file)

    
