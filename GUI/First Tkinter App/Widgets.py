import tkinter as tk

def increase():
    value = int(lbl_value["text"])
    lbl_value["text"] = f"{value + 1}"

def decrease():
    value = int(lbl_value["text"])
    lbl_value["text"] = f"{value - 1}"
    
border_effects = {"flat": tk.FLAT,
                  "sunken": tk.SUNKEN,
                  "raised": tk.RAISED,
                  "groove": tk.GROOVE,
                  "ridge": tk.RIDGE}
window = tk.Tk()

# Label Widgets

label = tk.Label(text="Enter your name",
                 fg="white",
                 bg="black",
                 width=30,
                 height=1)
label.pack()

# Entry Widgets

entry = tk.Entry(fg="black", bg="white", width=50 )
entry.pack()

# Button Widgets

button = tk.Button(text="Ok",
                   width=3,
                   height=1,
                   bg="grey",
                   fg="black")
button.pack()

# Text Entry

#text_box = tk.Text()
#text_box.pack()

# Assigning Widgets to Frames

##frame = tk.Frame()
##frame.pack()
##
##label1 = tk.Label(master=frame)
##
##for relief_name, relief in border_effects.items():
##    frame = tk.Frame(master=window, relief=relief, borderwidth=5)
##    frame.pack(side=tk.LEFT)
##    label = tk.Label(master=frame, text=relief_name)
##    label.pack()

button = tk.Button(text="Click here",
                   width=30,
                   height=1,
                   bg="white",
                   fg="blue")
button.pack()

# event handler

def handle_click(event):
    print("The button was clicked!")
button.bind("<Button-1>", handle_click)

frame2 = tk.Frame(master=window, width=30, height=30, bg="red")
frame2.pack()

frame3 = tk.Frame(master=window, width=30, height=30, bg="yellow")
frame3.pack()

window1 = tk.Tk()

# command Attribute

window1.rowconfigure(0, minsize=50, weight=1)
window1.columnconfigure([0, 1, 2], minsize=50, weight=1)

btn_decrease = tk.Button(master=window1, text="-", command=decrease)
btn_decrease.grid(row=0, column=0, sticky="nsew")

lbl_value = tk.Label(master=window1, text="0")
lbl_value.grid(row=0, column=1)

btn_increase = tk.Button(master=window1, text="+", command=increase)
btn_increase.grid(row=0, column=2, sticky="nsew")

label1 = tk.Label(text="Hello")
text = label1["text"]
label1["text"] = "Good bye"
   
window.mainloop()




