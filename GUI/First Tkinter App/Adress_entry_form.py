import tkinter as tk

window = tk.Tk()

frame = tk.Frame(master=window, relief=tk.RAISED, borderwidth=1)
frame.pack(side=tk.LEFT)

labels = {"First Name:":tk.FLAT,
          "Last Name:":tk.FLAT,
          "Address Line 1:":tk.FLAT,
          "Address Line 2:":tk.FLAT,
          "City:":tk.FLAT,
          "State/Province:":tk.FLAT,
          "Postal Code:":tk.FLAT,
          "Country:":tk.FLAT}


for label_name in labels:
    label = tk.Label(master=frame, text=label_name)
    label.pack(padx=5, pady=5)

    
    entry1 = tk.Entry(fg="black", bg="white", width=50)
    entry1.pack(padx=5, pady=6)
    

events_list = []

def handle_keypress(event):
    print(event.char)

window.mainloop()
