""" button widget for opening a file for editing: btn_open

button widget for saving a file: btn_save

textbox widget for creating and editing the text file: txt_edit """

import tkinter as tk
from tkinter.filedialog import askopenfilename, asksaveasfilename
def open_file():
    """Open a file for editing."""
    # 1
    filepath = askopenfilename(
        filetypes=[("Text Files", "*.txt"), ("All Files", "*.*")]
        )
    # 2
    if not filepath:
        return
    # 3
    txt_edit.delete("1.0", tk.END)
    # 4
    with open(filepath, "r") as input_file:
        text = input_file.read()
        txt_edit.insert(tk.END, text)
    # 5
    window.title(f"Simple Text Editor - {filepath}")
    
def save_file():
    """Save a current file as a new file."""
    # 1
    filepath = asksaveasfilename(
        defaultextension="txt",
        filetypes=[("Text Files", "*.txt"), ("All Files", "*.*")]
        )
    # 2
    if not filepath:
        return
    # 3
    with open(filepath, "w") as output_file:
        text = txt_edit.get("1.0", tk.END)
        output_file.write(text)
    # 4
    window.title(f"Simple Text Editor - {filepath}")


# Create the all of the widgets that we need:

# 1. We create a new window with the title "Simple Text Editor"

window = tk.Tk()
window.title("Simple Text Editor")

# 2. Set the row and column configurations

window.rowconfigure(0, minsize=800, weight=1)
window.columnconfigure(1, minsize=800, weight=1)

# 3. Four widgets are created

txt_edit = tk.Text(window)
fr_buttons = tk.Frame(window)

btn_open = tk.Button(fr_buttons, text="Open", command=open_file)
btn_save = tk.Button(fr_buttons, text="Save As...", command=save_file)

# 4. Assign the two buttons to the fr_buttons frame using the grid() geometry manager

btn_open.grid(row=0, column=0, sticky="ew", padx=5, pady=5)
btn_save.grid(row=1, column=0, sticky="ew", padx=5)

# 5. Set up the grid layout for the rest of the window - create a grid
    # with one row and two columns for window.
    # fr_buttons is placed in the first column and txt_edit in the second
    # column so that fr_buttons appears to the left of txt_edit in the window
    #layout

fr_buttons.grid(row=0, column=0, sticky="ns")
txt_edit.grid(row=0, column=1, sticky="nsew")

window.mainloop()
