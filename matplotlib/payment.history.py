from matplotlib import pyplot as plt
import numpy as np

xs = [1, 2, 3, 4, 5]
ys = np.arange(1, 6)

plt.xlabel("Years")
plt.ylabel("Amount of payment in EUR")
plt.title("Payments")

plt.plot(xs, ys)
plt.show()