from matplotlib import pyplot as plt
import numpy as np

days = np.arange(0,21)
other_site = np.arange(0,21)
real_python = other_site ** 2

# plt.plot(days, other_site)
# plt.plot(days, real_python)
# plt.show()

plt.plot(days, other_site)
plt.plot(days, real_python)
plt.xticks([0,5,10,15,20])
plt.xlabel("days")
plt.ylabel("amount of Python learned")
plt.title("Python Learned Reading Real Python vs Other Site")
plt.legend(["Other", "Real Python"])
plt.show()

plt.tra