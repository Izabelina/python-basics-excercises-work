from matplotlib import pyplot as plt

# plt.plot([1,2,3,4,5])
# plt.show()


# xs = [1,2,3,4,5]
# ys = [2,4,6,8,10]
# plt.plot(xs, ys)
# plt.show()

# xs = [1,2,3,4,5]
# ys = [-2,0,-1,2,-2]
# plt.plot(xs, ys, "g-o")
# plt.show()

# xs = [1,2,3,4]
# y1 = [0,2,3,4]
# y2 = [1,3,5,8]
# plt.plot(xs, y1, "g-o", xs, y2, "b-^")
# plt.show()


plt.plot([0,2,3,4], "g-o")
plt.plot([1,3,5,8], "b-^")
plt.show()