from pathlib import Path
from PyPDF2 import PdfFileMerger, PdfFileWriter, PdfFileReader

# ŁĄCZYMY 2 PLIKI

pdfs_dir = Path.cwd()

for path in pdfs_dir.glob("*.pdf"):   # nie ma pewności, że wypisał w dobrej kolejności
    print(path.name)

pdf_files = list(pdfs_dir.glob("*.pdf")) # dlatego sortujemy
pdf_files.sort()

for path in pdf_files: # i mamy pewność, że kolejność jest dobra
    print(path.name)

input_paths = [pdfs_dir / "merge1.pdf", pdfs_dir / "merge2.pdf"]
pdf_merger = PdfFileMerger()

for path in input_paths:
    pdf_merger.append(str(path))

with Path("concatenated.pdf").open(mode="wb") as output_file:
    pdf_merger.write(output_file)

# WSTAWIAMY POMIĘDZY 1 i 2 STRONĘ PDFa DODATKOWĄ STRONĘ Z PLKU merge3.pdf

con_pdf_path = pdfs_dir / "concatenated.pdf" # deklarujemy 2 zmienne ze ścieżkami 
merge3_path = pdfs_dir /"merge3.pdf"         # do plików

pdf_merger = PdfFileMerger()            # tworzymy obiekt klasy PdfFileMerger
pdf_merger.append(str(con_pdf_path))    # i dokładamy do niego string ze ścieżką pliku głównego

pdf_merger.merge(1, str(merge3_path))   # wywołać metodę .merge() na zmiennej pdf_merger
                                        # i wskazać w niej aktrybuty: indeks strony, przed którą chcemy wcisnąć
                                        # żądany plik oraz string ze świeżki żądanego pliku.

with Path("merge.pdf").open(mode="wb") as output_file:
    pdf_merger.write(output_file)
                                        






