from PyPDF2 import PdfFileReader
from pathlib import Path

# to create a new instance of the PdfFileReade class you'll need to path
# to the PDF file that you want to open. #To do that using the pathlib module:
pdf_path = (Path.home()/'PYTHON'/'python-basics-excercises-work'/'work-with-pdf-files'/'Pride_and_Prejudice.pdf')
# create the PDFFileReader instance:
pdf = PdfFileReader(str(pdf_path))
output_file_path = Path.home()/'PYTHON'/'python-basics-excercises-work'/'work-with-pdf-files'/"Pride_and_Prejudice.txt"

# .getNumPages() method returns number of pages contained in the PDF file
print(pdf.getNumPages())    

# .documentInfo attribute allow to access dome document information
print(pdf.documentInfo)

# not dictionary, you can access each item in documentInfo as an attribute:
print(pdf.documentInfo.title)

# extracting the text as a string with the PageObject instances .extractText() method
# Step 1. - get a PageObject with PdfFileReader.getPage():
first_page = pdf.getPage(0)     # .getPage() returns a PageObject
print(first_page)
print(type(first_page))
# extract the page's text with the PageObject.extractText() method:
print(first_page.extractText())

'''
for page in pdf.pages:
    print(page.extractText())
'''

with output_file_path.open(mode="w") as output_file:
    output_file.write(f"{pdf.documentInfo.title}\n"
                      f"Numbers of pages: {pdf.getNumPages()}\n\n")
    for page in pdf.pages:
        text = page.extractText()
        output_file.write(text)
