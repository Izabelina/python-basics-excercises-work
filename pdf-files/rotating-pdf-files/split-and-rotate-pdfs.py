from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

pdf_pth = Path.cwd() / "split_and_rotate.pdf"
pdf_reader = PdfFileReader(str(pdf_pth))
pdf_writer = PdfFileWriter()

for n in range(pdf_reader.getNumPages()):
    page = pdf_reader.getPage(n)
    page.rotateClockwise(270)
    pdf_writer.addPage(page)

with Path("rotated.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)


import copy

pdf_pth = Path.cwd() / "rotated.pdf"

pdf_reader = PdfFileReader(str(pdf_pth))
pdf_writer = PdfFileWriter()

for page in pdf_reader.pages:
    upper_right_coords = page.mediaBox.upperRight
    center_coords = (upper_right_coords[0] / 2, upper_right_coords[1])
    left_page = copy.deepcopy(page)
    rigth_page = copy.deepcopy(page)
    left_page.mediaBox.upperRight = center_coords
    rigth_page.mediaBox.upperLeft = center_coords
    pdf_writer.addPage(left_page)
    pdf_writer.addPage(rigth_page)

output_path = Path.cwd() / "split.pdf"
with output_path.open(mode='wb') as output_file:
    pdf_writer.write(output_file)

