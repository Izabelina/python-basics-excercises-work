from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

# create a Path object for the ugly.pdf file

pdf_path = Path.cwd() / "ugly.pdf"

# create new instances PdFileReader and PdfFileWriter

pdf_reader = PdfFileReader(str(pdf_path))
pdf_writer = PdfFileWriter()

# first method - loop over the indices of the pages in the PDF

for n in range(pdf_reader.getNumPages()):
    page = pdf_reader.getPage(n)
    if n% 2 == 0:
        page.rotateClockwise(90)
    pdf_writer.addPage(page)

with Path("ugly_rotated.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)

# second method
# need creating new PdfFileReader instance to starting fresh

pdf_reader = PdfFileReader(str(pdf_path))
print(pdf_reader.getPage(0)) # value of the key called '/Rotate' is -90

page = pdf_reader.getPage(0)
print(page["/Rotate"]) # access the /Rotate key

page = pdf_reader.getPage(1)
print(page["/Rotate"])

pdf_reader = PdfFileReader(str(pdf_path))
pdf_writer = PdfFileWriter()

for page in pdf_reader.pages:
    if page["/Rotate"] == -90:
        page.rotateClockwise(90)
    pdf_writer.addPage(page)

with Path("ugly_rotated2.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)







