
>>> from PyPDF2 import PdfFileWriter
>>> pdf_writer = PdfFileWriter()
>>> page = pdf_writer.addBlankPage(width=72, height=72)
>>> type(page)
<class 'PyPDF2._page.PageObject'>
>>> page
{'/Type': '/Page', '/Parent': IndirectObject(1, 0, 2244772345232), '/Resources': {}, '/MediaBox': RectangleObject([0, 0, 72, 72])}
>>> from pathlib import Path
>>> with Path('blank.pdf').open(mode='wb') as output_file:
...     pdf_writer.write(output_file)
... 
...     
(False, <_io.BufferedWriter name='blank.pdf'>)

'''
w bieżącym katalogu utworzył się nowy PDF z pustą stroną o wymiarach 1 cala kwadratowego)
