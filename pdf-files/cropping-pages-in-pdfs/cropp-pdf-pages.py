from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

pdf_path = (Path.cwd() / "half_and_half.pdf") # create a Path object for the half_and_half.pdf file

pdf_reader = PdfFileReader(str(pdf_path)) # create a new PdfFileReader object and get the first page of the PDF
first_page = pdf_reader.getPage(0)

print(first_page.mediabox)

print(first_page.mediabox.lowerLeft)
print(first_page.mediabox.lowerRight)
print(first_page.mediabox.upperLeft)
print(first_page.mediabox.upperRight)

print(first_page.mediabox.upperRight[0])
print(first_page.mediabox.upperRight[1])

first_page.mediabox.upperLeft = (0, 480)
print(first_page.mediabox.upperLeft)

pdf_writer = PdfFileWriter()
pdf_writer.addPage((first_page))
with Path("cropped_page.pdf").open(mode="wb") as output_file:
    pdf_writer.write((output_file))

first_page = pdf_reader.getPage(0)

import copy

left_side = copy.deepcopy(first_page)
current_coords = left_side.mediaBox.upperRight
new_cords = ((current_coords[0]) / 2, current_coords[1])
left_side.mediaBox.upperRight = new_cords
right_side = copy.deepcopy(first_page)
right_side.mediaBox.upperLeft = new_cords
pdf_writer.addPage(left_side)
pdf_writer.addPage(right_side)
with Path("cropped_page.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)



