from PyPDF2 import PdfFileMerger

pdf_merger = PdfFileMerger()

from pathlib import Path

reports_dir = (Path.home() / 'PYTHON/python-basics-excercises-work/work-with-pdf-files/expense_reports')

for path in reports_dir.glob("*.pdf"):  # nie gwarantuje sortowania alfabetycznego
    print(path.name)

expense_list = list(reports_dir.glob("*.pdf"))  # sortuje alfabetycznie
expense_list.sort()

for path in expense_list:
    print(path.name)

# łączymy 3 pdfy w 1 plik:

for path in expense_list:
    pdf_merger.append(str(path))

with Path("expense_list.pdf").open(mode="wb") as output_file:
    pdf_merger.write(output_file)
