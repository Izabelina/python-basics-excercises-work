from PyPDF2 import PdfFileReader
from pathlib import Path

pdf_path = Path.home()/'PYTHON'/'python-basics-excercises-work'/'work-with-pdf-files'/'zen.pdf'
pdf = PdfFileReader(str(pdf_path))

print(pdf.getNumPages())

first_page = pdf.getPage(0)
print(first_page.extractText(0))