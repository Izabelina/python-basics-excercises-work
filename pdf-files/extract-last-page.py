from PyPDF2 import PdfFileReader, PdfFileWriter
from pathlib import Path

pdf_path = (Path.home()/'PYTHON'/'python-basics-excercises-work'/'work-with-pdf-files'/'Pride_and_Prejudice.pdf')
input_pdf = PdfFileReader(str(pdf_path))

#last_page = input_pdf.getPage(-1)

pdf_writer = PdfFileWriter()
pdf_reader = PdfFileReader(str(pdf_path))
#pdf_writer.addPage(last_page)

##with Path("last_page.pdf").open(mode='wb') as output_file:
##    pdf_writer.write(output_file)
##
##
##    
##for page in input_pdf.pages[::2]:
##    pdf_writer.addPage(page)
##
##output_path = (Path.home()/'PYTHON'/'python-basics-excercises-work'/'work-with-pdf-files'/'even_pages.pdf')
##with Path("even_pages.pdf").open(mode='wb') as output_file:
##    pdf_writer.write(output_file)

for n in range(150):
    page = input_pdf.getPage(n)
    pdf_writer.addPage(page)

print(pdf_writer.getNumPages())

with Path('part_1.pdf').open(mode='wb') as output_file:
    pdf_writer.write(output_file)

part_2 = PdfFileWriter()
part_2_pages = pdf_reader.pages[150:]

for n in part_2_pages:
    part_2.addPage(n)
    
with Path('part_2.pdf').open(mode='wb') as output_file:
    part_2.write(output_file)
    
