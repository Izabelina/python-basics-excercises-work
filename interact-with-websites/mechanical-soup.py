import mechanicalsoup

browser = mechanicalsoup.Browser()
url = "http://olympus.realpython.org/login"
page = browser.get(url)
print(page.soup)

login_page = page
login_html = login_page.soup

form = login_html.select("form")[0] #  returns a list of all <form> elements on the page
form.select("input")[0]["value"] = "zeus"
form.select("input")[1]["value"] = "ThunderDude"

profiles_page = browser.submit(form, login_page.url)

title = profiles_page.soup.title
print(f"Title: {title.text}")

login_page = browser.get(url)
login_title = login_page.soup.title
print(f"Title: {login_title.text}")

form = login_html.form
form.select("input")[0]["value"] = "wrong"
form.select("input")[1]["value"] = "password"
error_page = browser.submit(form, login_page.url)

if error_page.soup.text.find("Wrong username or password!") != -1:
    print("Login Failed.")
else:
    print("Login Successful")
