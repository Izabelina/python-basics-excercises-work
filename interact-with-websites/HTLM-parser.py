from bs4 import BeautifulSoup
from urllib.request import urlopen

'''
url = "http://olympus.realpython.org/profiles/dionysus"
page = urlopen(url)
html = page.read().decode("utf-8")
soup = BeautifulSoup(html, "html.parser")

print(soup.get_text())

print(soup.find_all("img"))

image1, image2 = (soup.find_all("img"))
print(image1.name)
print(image1["src"])
print(image2["src"])
print(soup.title)   # get the <title> tag in a document
print(soup.title.string)    # retrieve just the string in the title tag with the .string property of the Tag object
'''

print("     ")

url1 = "http://olympus.realpython.org/profiles"
page1 = urlopen(url1)
html1 = page1.read().decode("utf-8")
print(html1)

soup1 = BeautifulSoup(html1, "html.parser")
print(soup1.find_all("a"))
a1, a2, a3 = soup1.find_all("a")
print(a1["href"])
print(a2["href"])
print(a3["href"])

links_list = [a1["href"], a2["href"], a3["href"]]
print(links_list)
for a in links_list:
    print(a)
    
