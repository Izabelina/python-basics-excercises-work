from urllib.request import urlopen

url_aphrodite = 'http://olympus.realpython.org/profiles/aphrodite'

html_page = urlopen(url_aphrodite)
html_text = html_page.read().decode('utf-8')

print(html_text)

page = urlopen(url_aphrodite)
html = page.read().decode('utf-8')
start_tag = "<title>"
end_tag = "</title>"
start_index = html.find(start_tag) + len(start_tag)
end_index = html.find(end_tag)

print(html[start_index:end_index])
                                                
url_poseidon = 'http://olympus.realpython.org/profiles/poseidon'

html_page = urlopen(url_poseidon)
html_text_poseidon = html_page.read().decode('utf-8')

print(html_text_poseidon)

page = urlopen(url_poseidon)
html = page.read().decode('utf-8')
start_tag = "<title>"
end_tag = "</title>"
start_index = html.find(start_tag) + len(start_tag)
end_index = html.find(end_tag)

print(html[start_index:end_index])

import re

print(re.findall("ab*c","abcd"))

# 3 parse text from websites



url3 = "http://olympus.realpython.org/profiles/dionysus"
page = urlopen(url3)
html = page.read().decode("utf-8")

pattern = "<title.*?>.*?</title.*?>"
match_result = re.search(pattern, html, re.IGNORECASE)
title = match_result.group()
title = re.sub("<.*?>", "", title) #remove THML tags

print(title)


