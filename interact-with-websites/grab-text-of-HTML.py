# 1. Write a script that grabs the full HTML from the page http://olympus.realpython.org/profiles/dionysus

from urllib.request import urlopen

url = "http://olympus.realpython.org/profiles/dionysus"
html_page = urlopen(url)
html_text = html_page.read().decode("utf-8")
#print(html_text)

# 2. Use the string .find() method to display the text following “Name:” and “Favorite Color:” (not including any leading spaces or trailing HTML tags that might appear on the same line).

start_tag1 = "<h2>"
end_tag1 = "</h2>"

start_index1 = html_text.find(start_tag1) + len(start_tag1)+6
end_index1 = html_text.find(end_tag1)

print(html_text[start_index1:end_index1])

start_tag2 = "</center>"
end_tag2 = "</center>"

start_index2 = html_text.find(start_tag2) - len(start_tag2)+4
end_index2 = html_text.find(end_tag2)

print(html_text[start_index2:end_index2])

# 3. Regular expressions

import re

pattern = "<h2.*?>.*?</h2.*?>"
match_results = re.search(pattern, html_text, re.IGNORECASE)
h2 = match_results.group()
h2 = re.sub("<.*?>", "", h2)

print(h2)

match_results = re.findall("Wine.*", html_text, re.IGNORECASE)
print(str(match_results))
