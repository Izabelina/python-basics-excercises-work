import mechanicalsoup

# script that creates a new Browser instance with Mechanical Soup
# and retrives a webpage:

browser = mechanicalsoup.Browser()
url = "http://olympus.realpython.org/login"
page = browser.get(url)
print(page.soup)

# script that fills the form out and submits it

# 1

login_page = page
login_html = login_page.soup

# 2 filling out the form
# retrieve the <form> element itself from the page’s HTML
# select the username and password inputs and set their value to
# "zeus" and "ThunderDude", respectivel

form = login_html.select("form")[0] #  returns a list of all <form> elements on the page
form.select("input")[0]["value"] = "zeus"
form.select("input")[1]["value"] = "ThunderDude"

# 3 form is submitted with the browser.submit() method.

profiles_page = browser.submit(form, login_page.url)

# how to programmatically obtain the URL for each link on the /profiles page.

links = profiles_page.soup.select("a")
for link in links:
    address = link["href"]
    text = link.text
    print(f"{text}:{address}")
    
base_url = "http://olympus.realpython.org"
for link in links:
    address = base_url + link["href"]
    text = link.text
    print(f"{text}:{address}")
