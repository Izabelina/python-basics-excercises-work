number = 1

while number < 32:
    if number == 18:
        break

    if number % 3 == 0:
        print('bomba')
        number += 1
        continue

    print(number)
    number += 1

list = []
a = []
for i in range(3):
    a = [5 for i in range(3)]
    list.append(a)
for elem in list:
    print(elem)

word = "good morning"

print(word.upper())

list = [1,2,3,4,4]
a = [3,3]
b = list * 2

print(list)
print(list[:-1])
print(list[-1:])
print(list[3::])
print(list[3::-1])

sentence = "Ala ma kota"
sentence = sentence.split()
print(sentence)

list.insert(1, 100)
print(list)

print(list.index(100))
print(max(list))
print(min(list))

print(list.count(4))
list.remove(100)
print(list)

list.reverse()
print(list)

if "ma" in sentence:
    print("yes")

zdanie = "Piszę jakieś tam zdanie"

if "tam" in zdanie:
    print("yes")

print(zdanie.count("i"))
licznik = 0
for litera in zdanie:
    if "i" == litera:
        licznik += 1
print(licznik)

zdanie = " ".join(sentence)

print(zdanie.replace("kota", 'psa'))
zdanie = zdanie.replace("kota", 'psa')
print(zdanie.startswith("Ala"))
zdanie = zdanie.lower()
print(zdanie)

print(zdanie.find("ma"))

print(f"Wypisz zdanie: {zdanie}")

print(sum(list))

print(all([i > 5 for i in list]))
print(any([i % 2 == 0 for i in list]))




