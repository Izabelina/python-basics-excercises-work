Python 3.11.0 (main, Oct 24 2022, 18:26:48) [MSC v.1933 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
import pathlib
path = pathlib.Path("/Users/Admin/PYTHON/python-basics-exercises-master/ch14-interact-with-pdf-files/practice_files/Pride_and_Prejudice.pdf")
path
WindowsPath('/Users/Admin/PYTHON/python-basics-exercises-master/ch14-interact-with-pdf-files/practice_files/Pride_and_Prejudice.pdf')
home = pathlib.Path.home()
home
WindowsPath('C:/Users/Admin')
pathlib.Path.cwd()
WindowsPath('C:/Users/Admin/AppData/Local/Programs/Python/Python311')
home/"PYTHON"/"python-basics-exercises-master"/"ch14-interact-with-pdf-files"/"practice_files"/"Pride_and_Prejudice"
WindowsPath('C:/Users/Admin/PYTHON/python-basics-exercises-master/ch14-interact-with-pdf-files/practice_files/Pride_and_Prejudice')
home/"PYTHON"/"ch14-interact-with-pdf-files"/"practice_files"/"Pride_and_Prejudice"
WindowsPath('C:/Users/Admin/PYTHON/ch14-interact-with-pdf-files/practice_files/Pride_and_Prejudice')
home/"PYTHON"/"practice_files"/"Pride_and_Prejudice"
WindowsPath('C:/Users/Admin/PYTHON/practice_files/Pride_and_Prejudice')
path = pathlib.Path(r"practice_files/Pride_and_Prejudice.pdf")
path
WindowsPath('practice_files/Pride_and_Prejudice.pdf')
path.is_absolute()
False
home = pathlib.Path.home()
home/pathlib.Path(r"practice_files/Pride_and_Prejudice.pdf")
WindowsPath('C:/Users/Admin/practice_files/Pride_and_Prejudice.pdf')
path = pathlib.Path.home() / "Pride_and_Prejudice.pdf"
path
WindowsPath('C:/Users/Admin/Pride_and_Prejudice.pdf')
list(path.parents)
[WindowsPath('C:/Users/Admin'), WindowsPath('C:/Users'), WindowsPath('C:/')]
path = pathlib.Path.home() / "PYTHON"/"python-basics-exercises-master"/"ch14-interact-with-pdf-files"/"practice_files"/"Pride_and_Prejudice"
path
WindowsPath('C:/Users/Admin/PYTHON/python-basics-exercises-master/ch14-interact-with-pdf-files/practice_files/Pride_and_Prejudice')
list(path.parents)
[WindowsPath('C:/Users/Admin/PYTHON/python-basics-exercises-master/ch14-interact-with-pdf-files/practice_files'), WindowsPath('C:/Users/Admin/PYTHON/python-basics-exercises-master/ch14-interact-with-pdf-files'), WindowsPath('C:/Users/Admin/PYTHON/python-basics-exercises-master'), WindowsPath('C:/Users/Admin/PYTHON'), WindowsPath('C:/Users/Admin'), WindowsPath('C:/Users'), WindowsPath('C:/')]
for directory in path.parents:
    print(directory)

    
C:\Users\Admin\PYTHON\python-basics-exercises-master\ch14-interact-with-pdf-files\practice_files
C:\Users\Admin\PYTHON\python-basics-exercises-master\ch14-interact-with-pdf-files
C:\Users\Admin\PYTHON\python-basics-exercises-master
C:\Users\Admin\PYTHON
C:\Users\Admin
C:\Users
C:\
path.anchor
'C:\\'
relpath = pathlib.Path("Pride_and_Prejudice")
relpath.anchor
''
home.name
'Admin'
path = home / 'Pride_and_Prejudice'
path.name
'Pride_and_Prejudice'
path.exists()
False
path.is_file()
False
path = pathlib.Path.home() / 'PYTHON'/'python-basics-exercises-master'/'ch14-interact-with-pdf-files'/'practice_files'/'Pride_and_Prejudice.pdf'
path.exists()
True
path.is_file()
True
path.is_dir()
False
home.is_dir()
True
path = pathlib.Path.home() / 'PYTHON'/'python-basics-exercises-master'/'ch14-interact-with-pdf-files'/'practice_files'
path.is_dir()
True
new_path = pathlib.Path.home() / "my_folder"/"my_file.txt"
new_path
WindowsPath('C:/Users/Admin/my_folder/my_file.txt')
new_path.exists
<bound method Path.exists of WindowsPath('C:/Users/Admin/my_folder/my_file.txt')>
new_path.exists()
False
new_path.name
'my_file.txt'
new_path.parent()
Traceback (most recent call last):
  File "<pyshell#43>", line 1, in <module>
    new_path.parent()
TypeError: 'WindowsPath' object is not callable
new_path.parents
<WindowsPath.parents>
new_path.parents()
Traceback (most recent call last):
  File "<pyshell#45>", line 1, in <module>
    new_path.parents()
TypeError: '_PathParents' object is not callable
new_path.parent()
Traceback (most recent call last):
  File "<pyshell#46>", line 1, in <module>
    new_path.parent()
TypeError: 'WindowsPath' object is not callable

new_path.parent
WindowsPath('C:/Users/Admin/my_folder')
new_path.parent.name
'my_folder'
from pathlib import Path
new_dir = Path.home() / "new_directory"
new_dir.mkdir()
new_dir.exists()
True
new_dir.is_dir()
True
new_dir.mkdir()
Traceback (most recent call last):
  File "<pyshell#54>", line 1, in <module>
    new_dir.mkdir()
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\pathlib.py", line 1116, in mkdir
    os.mkdir(self, mode)
FileExistsError: [WinError 183] Nie można utworzyć pliku, który już istnieje: 'C:\\Users\\Admin\\new_directory'
if not new_dir.exists():
    new_dir.mkdir()

    
nested_dir = new_dir / "folder_a" / "folder_b"

nested_dir.exists()
False
neted_dir.mkdir()
Traceback (most recent call last):
  File "<pyshell#61>", line 1, in <module>
    neted_dir.mkdir()
NameError: name 'neted_dir' is not defined. Did you mean: 'nested_dir'?
nested_dir.mkdir()
Traceback (most recent call last):
  File "<pyshell#62>", line 1, in <module>
    nested_dir.mkdir()
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\pathlib.py", line 1116, in mkdir
    os.mkdir(self, mode)
FileNotFoundError: [WinError 3] System nie może odnaleźć określonej ścieżki: 'C:\\Users\\Admin\\new_directory\\folder_a\\folder_b'
nested_dir.mkdir(parents=True)
path.mkdir(parents=True, exists_ok=True)
Traceback (most recent call last):
  File "<pyshell#64>", line 1, in <module>
    path.mkdir(parents=True, exists_ok=True)
TypeError: Path.mkdir() got an unexpected keyword argument 'exists_ok'
path.mkdir(parents=True, exist_ok=True)
nested_dir.exists()
True
file_path = new_dir / "file1.txt"
file_path
WindowsPath('C:/Users/Admin/new_directory/file1.txt')
file_path.exists()
False
file_path.touch()
file_path.exists()
True
file_path.ispath()
Traceback (most recent call last):
  File "<pyshell#72>", line 1, in <module>
    file_path.ispath()
AttributeError: 'WindowsPath' object has no attribute 'ispath'
file_path.isfile()
Traceback (most recent call last):
  File "<pyshell#73>", line 1, in <module>
    file_path.isfile()
AttributeError: 'WindowsPath' object has no attribute 'isfile'. Did you mean: 'is_file'?
file_path.is_file()
True
file_path.paremt.mkdir()
Traceback (most recent call last):
  File "<pyshell#75>", line 1, in <module>
    file_path.paremt.mkdir()
AttributeError: 'WindowsPath' object has no attribute 'paremt'. Did you mean: 'parent'?
file_path.parent.mkdir()
Traceback (most recent call last):
  File "<pyshell#76>", line 1, in <module>
    file_path.parent.mkdir()
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\pathlib.py", line 1116, in mkdir
    os.mkdir(self, mode)
FileExistsError: [WinError 183] Nie można utworzyć pliku, który już istnieje: 'C:\\Users\\Admin\\new_directory'
file_path = new_dir / "folder_c" / "file2.txt"
file_path.parent.mkdir()
file_path.parents()
Traceback (most recent call last):
  File "<pyshell#79>", line 1, in <module>
    file_path.parents()
TypeError: '_PathParents' object is not callable
file_path.is_dir()
False
file_path.parent.name()
Traceback (most recent call last):
  File "<pyshell#81>", line 1, in <module>
    file_path.parent.name()
TypeError: 'str' object is not callable
for path in new_dir.iterdir():
    print(path)

    
C:\Users\Admin\new_directory\file1.txt
C:\Users\Admin\new_directory\folder_a
C:\Users\Admin\new_directory\folder_c
list(new_dir.iterdir())
[WindowsPath('C:/Users/Admin/new_directory/file1.txt'), WindowsPath('C:/Users/Admin/new_directory/folder_a'), WindowsPath('C:/Users/Admin/new_directory/folder_c')]
paths = [
new_dir / "program1.py",
new_dir / "program2.py",
new_dir / "folder_a" / "program3.py",
new_dir / "folder_a" / "folder_b" / "image1.jpg",
new_dir / "folder_a" / "folder_b" / "image2.png",
]

for path in paths:
    path.touch()

    
list(new_dir.glob("*.py"))
[WindowsPath('C:/Users/Admin/new_directory/program1.py'), WindowsPath('C:/Users/Admin/new_directory/program2.py')]
list(new_dir.glob("*1*"))
[WindowsPath('C:/Users/Admin/new_directory/file1.txt'), WindowsPath('C:/Users/Admin/new_directory/program1.py')]
list(new_dir.glob("*program?.py*"))
[WindowsPath('C:/Users/Admin/new_directory/program1.py'), WindowsPath('C:/Users/Admin/new_directory/program2.py')]
list(new_dir.glob("**/*.py"))
[WindowsPath('C:/Users/Admin/new_directory/program1.py'), WindowsPath('C:/Users/Admin/new_directory/program2.py'), WindowsPath('C:/Users/Admin/new_directory/folder_a/program3.py')]
[WindowsPath('C:/Users/Admin/new_directory/program1.py'), WindowsPath('C:/Users/Admin/new_directory/program2.py'), WindowsPath('C:/Users/Admin/new_directory/folder_a/program3.py')]
Traceback (most recent call last):
  File "<pyshell#94>", line 1, in <module>
    [WindowsPath('C:/Users/Admin/new_directory/program1.py'), WindowsPath('C:/Users/Admin/new_directory/program2.py'), WindowsPath('C:/Users/Admin/new_directory/folder_a/program3.py')]
NameError: name 'WindowsPath' is not defined
for pathin new_dir.itedir():
    
SyntaxError: invalid syntax
for path in new_dir.iterdir():
    print(path)

    
C:\Users\Admin\new_directory\file1.txt
C:\Users\Admin\new_directory\folder_a
C:\Users\Admin\new_directory\folder_c
C:\Users\Admin\new_directory\program1.py
C:\Users\Admin\new_directory\program2.py
source = new_dir / "file1.txt"
destin = new-dir / "folder_a" / "file1.txt"
Traceback (most recent call last):
  File "<pyshell#101>", line 1, in <module>
    destin = new-dir / "folder_a" / "file1.txt"
NameError: name 'new' is not defined
destin = new_dir / "folder_a" / "file1.txt"
source.replace(destin)
WindowsPath('C:/Users/Admin/new_directory/folder_a/file1.txt')
file_to_unlink_path = new_dir / "program1.py"
file_to_unlink_path.unlink()

file_to_unlink_path.exists()
False
for path in new_dir.iterdir():
    print(path)

    
C:\Users\Admin\new_directory\folder_a
C:\Users\Admin\new_directory\folder_c
C:\Users\Admin\new_directory\program2.py
folder_d = new_dir / "folder_d"
for path in new_dir.iterdir():
    print(path)

    
C:\Users\Admin\new_directory\folder_a
C:\Users\Admin\new_directory\folder_c
C:\Users\Admin\new_directory\program2.py

folder_d.mkdir()
for path in new_dir.iterdir():
    print(path)

    
C:\Users\Admin\new_directory\folder_a
C:\Users\Admin\new_directory\folder_c
C:\Users\Admin\new_directory\folder_d
C:\Users\Admin\new_directory\program2.py
foder_d.rmdir()
Traceback (most recent call last):
  File "<pyshell#117>", line 1, in <module>
    foder_d.rmdir()
NameError: name 'foder_d' is not defined. Did you mean: 'folder_d'?
folder_d.rmdir()

for path in new_dir.iterdir():
    print(path)

    
C:\Users\Admin\new_directory\folder_a
C:\Users\Admin\new_directory\folder_c
C:\Users\Admin\new_directory\program2.py
for path in folder_a.iterdir():
    path.unlink()

    
Traceback (most recent call last):
  File "<pyshell#125>", line 1, in <module>
    for path in folder_a.iterdir():
NameError: name 'folder_a' is not defined. Did you mean: 'folder_d'?
for path in folder_a.iterdir():
    path.unlink()

    
Traceback (most recent call last):
  File "<pyshell#127>", line 1, in <module>
    for path in folder_a.iterdir():
NameError: name 'folder_a' is not defined. Did you mean: 'folder_d'?
for path in folder_d.iterdir():
    path.unlink()

    
Traceback (most recent call last):
  File "<pyshell#129>", line 1, in <module>
    for path in folder_d.iterdir():
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\pathlib.py", line 931, in iterdir
    for name in os.listdir(self):
FileNotFoundError: [WinError 3] System nie może odnaleźć określonej ścieżki: 'C:\\Users\\Admin\\new_directory\\folder_d'
folder_d.exists()
False
folder_a.exists()
Traceback (most recent call last):
  File "<pyshell#131>", line 1, in <module>
    folder_a.exists()
NameError: name 'folder_a' is not defined. Did you mean: 'folder_d'?
import shutil
folder_a = new_dir / "folder_a"
shutil.rmtree(folder_a)
folder_a.exists()
False
new_python_ex = new_dir / "PYTHON" / "python-basics-exercises"
new_python_ex.mkdir()
Traceback (most recent call last):
  File "<pyshell#137>", line 1, in <module>
    new_python_ex.mkdir()
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\pathlib.py", line 1116, in mkdir
    os.mkdir(self, mode)
FileNotFoundError: [WinError 3] System nie może odnaleźć określonej ścieżki: 'C:\\Users\\Admin\\new_directory\\PYTHON\\python-basics-exercises'
new_dir = Path.home() / PYTHON / "python-basics-excercises"
Traceback (most recent call last):
  File "<pyshell#138>", line 1, in <module>
    new_dir = Path.home() / PYTHON / "python-basics-excercises"
NameError: name 'PYTHON' is not defined
new_dir = Path.home() / 'PYTHON' / "python-basics-excercises"
new_dir.mkdir()
file_path = new_dir / "PYTHON" / "python-basics-exercises" / "file1.txt"
file_path.touch()
Traceback (most recent call last):
  File "<pyshell#142>", line 1, in <module>
    file_path.touch()
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\pathlib.py", line 1108, in touch
    fd = os.open(self, flags, mode)
FileNotFoundError: [Errno 2] No such file or directory: 'C:\\Users\\Admin\\PYTHON\\python-basics-excercises\\PYTHON\\python-basics-exercises\\file1.txt'
file_path.exists()
False
file_path = new_dir / "file1.txt"
file_path.touch()
file_path.exists()
True
file2_path = new_dir / "file2.txt"
file2_path.touch()
file2_path.exists()
True
file3_path = new_dir / "image1.png"
file3_path.exists()
False
file3_path.touch()
file3_path.exists()
True
work_dir = Path.home() / 'PYTHON' / "python-basics-excercises" / "work-with-dir-and-files"
wor_dir.mkdir()
Traceback (most recent call last):
  File "<pyshell#155>", line 1, in <module>
    wor_dir.mkdir()
NameError: name 'wor_dir' is not defined. Did you mean: 'work_dir'?
work_dir.mkdir()
work_dir.exists()
True
images_dir = work_dir / "images/"
images_dir.mkdir()
images_dir.exists()
True
source = work_dir / "images.png"
destin = omages_dir / "images.png"
Traceback (most recent call last):
  File "<pyshell#162>", line 1, in <module>
    destin = omages_dir / "images.png"
NameError: name 'omages_dir' is not defined. Did you mean: 'images_dir'?
destin = images_dir / "images.png"
source.replace(destin)
Traceback (most recent call last):
  File "<pyshell#164>", line 1, in <module>
    source.replace(destin)
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\pathlib.py", line 1188, in replace
    os.replace(self, target)
FileNotFoundError: [WinError 2] Nie można odnaleźć określonego pliku: 'C:\\Users\\Admin\\PYTHON\\python-basics-excercises\\work-with-dir-and-files\\images.png' -> 'C:\\Users\\Admin\\PYTHON\\python-basics-excercises\\work-with-dir-and-files\\images\\images.png'
source = work_dir / "image1.png"
destin = images_dir / "image1.png"
source.replace(destin)
Traceback (most recent call last):
  File "<pyshell#167>", line 1, in <module>
    source.replace(destin)
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\pathlib.py", line 1188, in replace
    os.replace(self, target)
FileNotFoundError: [WinError 2] Nie można odnaleźć określonego pliku: 'C:\\Users\\Admin\\PYTHON\\python-basics-excercises\\work-with-dir-and-files\\image1.png' -> 'C:\\Users\\Admin\\PYTHON\\python-basics-excercises\\work-with-dir-and-files\\images\\image1.png'
destin = work_dir / "images_dir" / "image1.png"
source.replace(destin)
Traceback (most recent call last):
  File "<pyshell#169>", line 1, in <module>
    source.replace(destin)
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\pathlib.py", line 1188, in replace
    os.replace(self, target)
FileNotFoundError: [WinError 2] Nie można odnaleźć określonego pliku: 'C:\\Users\\Admin\\PYTHON\\python-basics-excercises\\work-with-dir-and-files\\image1.png' -> 'C:\\Users\\Admin\\PYTHON\\python-basics-excercises\\work-with-dir-and-files\\images_dir\\image1.png'
source = work_dir.parent() / "image1.png"
Traceback (most recent call last):
  File "<pyshell#170>", line 1, in <module>
    source = work_dir.parent() / "image1.png"
TypeError: 'WindowsPath' object is not callable
source = Path.home() / 'PYTHON' / "python-basics-excercises" / "image1.png"
destin = work_dir / "images_dir" / "image1.png"
destin = work_dir / "images" / "image1.png"
destin = images_dir / "image1.png"
image1.replace(images_dir / "image1.png")
Traceback (most recent call last):
  File "<pyshell#175>", line 1, in <module>
    image1.replace(images_dir / "image1.png")
NameError: name 'image1' is not defined
images_dir = work_dir / "images/"
images_dir.mkdir()
Traceback (most recent call last):
  File "<pyshell#177>", line 1, in <module>
    images_dir.mkdir()
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\pathlib.py", line 1116, in mkdir
    os.mkdir(self, mode)
FileExistsError: [WinError 183] Nie można utworzyć pliku, który już istnieje: 'C:\\Users\\Admin\\PYTHON\\python-basics-excercises\\work-with-dir-and-files\\images'
image1.replace(images_dir / "image1.png")
Traceback (most recent call last):
  File "<pyshell#178>", line 1, in <module>
    image1.replace(images_dir / "image1.png")
NameError: name 'image1' is not defined
>>> file3_path.replace(images_dir / "image1.png")
WindowsPath('C:/Users/Admin/PYTHON/python-basics-excercises/work-with-dir-and-files/images/image1.png')
>>> file2_path.replace(images_dir / "file1.txt")
WindowsPath('C:/Users/Admin/PYTHON/python-basics-excercises/work-with-dir-and-files/images/file1.txt')
>>> file1_path.replace(images_dir / "file1.txt")
Traceback (most recent call last):
  File "<pyshell#181>", line 1, in <module>
    file1_path.replace(images_dir / "file1.txt")
NameError: name 'file1_path' is not defined. Did you mean: 'file_path'?
>>> file1_path.replace(images_dir / "file2.txt")
Traceback (most recent call last):
  File "<pyshell#182>", line 1, in <module>
    file1_path.replace(images_dir / "file2.txt")
NameError: name 'file1_path' is not defined. Did you mean: 'file_path'?
>>> file2_path.replace(images_dir / "file2.txt")
Traceback (most recent call last):
  File "<pyshell#183>", line 1, in <module>
    file2_path.replace(images_dir / "file2.txt")
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\pathlib.py", line 1188, in replace
    os.replace(self, target)
FileNotFoundError: [WinError 2] Nie można odnaleźć określonego pliku: 'C:\\Users\\Admin\\PYTHON\\python-basics-excercises\\file2.txt' -> 'C:\\Users\\Admin\\PYTHON\\python-basics-excercises\\work-with-dir-and-files\\images\\file2.txt'
>>> file_path.replace(images_dir / "file1.txt")
WindowsPath('C:/Users/Admin/PYTHON/python-basics-excercises/work-with-dir-and-files/images/file1.txt')
>>> file1_path = images_dir / "file1.txt")
SyntaxError: unmatched ')'
>>> file1_path = images_dir / "file1.txt"
>>> file1_path
WindowsPath('C:/Users/Admin/PYTHON/python-basics-excercises/work-with-dir-and-files/images/file1.txt')
>>> file1_path.unlink()
>>> file1_path
WindowsPath('C:/Users/Admin/PYTHON/python-basics-excercises/work-with-dir-and-files/images/file1.txt')
>>> file1_path.exists()
False
>>> import shutil
>>> fold = images_dir / "images"
>>> shutil.rmtree(fold)
Traceback (most recent call last):
  File "<pyshell#193>", line 1, in <module>
    shutil.rmtree(fold)
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\shutil.py", line 758, in rmtree
    return _rmtree_unsafe(path, onerror)
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\shutil.py", line 602, in _rmtree_unsafe
    onerror(os.scandir, path, sys.exc_info())
  File "C:\Users\Admin\AppData\Local\Programs\Python\Python311\Lib\shutil.py", line 599, in _rmtree_unsafe
    with os.scandir(path) as scandir_it:
FileNotFoundError: [WinError 3] System nie może odnaleźć określonej ścieżki: 'C:\\Users\\Admin\\PYTHON\\python-basics-excercises\\work-with-dir-and-files\\images\\images'
fold = images
Traceback (most recent call last):
  File "<pyshell#194>", line 1, in <module>
    fold = images
NameError: name 'images' is not defined
fol = images_dir
shutil.rmtree(fol)
