import pathlib
from pathlib import Path
work_dir = Path.home() / 'PYTHON'/'python-basics-excercises'/'work-with-dir-and-files'
work_dir
WindowsPath('C:/Users/Admin/PYTHON/python-basics-excercises/work-with-dir-and-files')

path = work_dir / "starship.txt"

list_of_ships = [
'Discovery\n',
'Enterprise\n',
'Defiant\n',
'Voyager\n']
    
with path.open(mode='w', encoding='utf-8') as file:
    file.writelines(list_of_ships)

    
with path.open(mode='r', encoding = 'utf-8')  as file:
    for line in file.readlines():
        print(line, end='')
    
Discovery
Enterprise
Defiant
Voyager

with path.open(mode='r', encoding = 'utf-8')  as file:
    for line in file.readlines():
        if line.startswith('D'):
            print(line, end='')
  
Discovery
Defiant
