import sqlite3

# create a new database
with sqlite3.connect("new_database.db") as connection:
    cursor = connection.cursor()
    cursor.execute("DROP TABLE IF EXISTS Roster")
    cursor.execute(
        """CREATE TABLE Roster(Name TEXT, Species TEXT, IQ INT);"""
        )

# complete the table with values
    values = [
        ("Jean Zorg", "Human", 122),
        ("Korben Dallas", "Meat Popsicle", 100),
        ("Ak'not", "Manga", -5)]
    cursor.executemany(
        "INSERT INTO Roster VALUES(?,?,?)", values
        )

# update the Species of Korben Dallas to be Human
    cursor.execute(
        "UPDATE Roster SET Species=? WHERE Name=? AND IQ=?",
        ("Human", "Korben Dallas", 100)
        )

    cursor.execute(
        "SELECT Name, IQ FROM Roster WHERE Species == 'Human'"
        )
# display the names and Iqs of everyone in the table classified as Human
    for row in cursor.fetchall():
        print(row)
