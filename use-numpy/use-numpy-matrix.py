# create numpy array

import numpy as np
matrix = np.array([[1,2,3],[4,5,6],[7,8,9]])
print(matrix)

# array operations

print(2 * matrix)

matrix2 = np.array([[5,4,3],[7,6,5],[9,8,7]])
print(matrix2-matrix)

# tuple of axis length

print(matrix.shape)

        
# array of the diagonal entries

print(matrix.diagonal())

# transpose of an array

print(matrix.transpose())

# calculate minimum entry

print(matrix.min())
print(matrix.max())

# stacking and shaping Arrays

A = np.array([[1,2],[3,4]])
B = np.array([[10,20],[30,40]])

print(np.vstack([A, B]))
print(np.hstack([A, B]))

#reshape
print(A.reshape(4,1))

C = np.arange(1,10)
print(C)

matrix = matrix.reshape(3,3)
print(matrix)

print(np.arange(1,13).reshape(3,1,4))

D = np.array([1,2,3,4,5,6,7,8,9,10,11,12])
print(D.reshape(3,2,2))

E = np.arange(1,24,2).reshape(3,2,2)
print(E)

F = np.random.random([3,3])
print(F)
