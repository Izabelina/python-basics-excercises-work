num = 10

while num < 161:
    num = num * 2
    print(num)

print("\n*******************\n")

s = 'hello'

for char in s:
    print(char)

print('\n')

i = 0

while not (s[i] in 'aoeiouAOEIOU'):
    print(s[i])
    i = i + 1

print(i)

print('\n')
    
'''
s1 = 'xyz' - znaki w każdym indeksie od s nie są samogłoskami,
długość ciągu s1 = 3 natomiast najwyższy indeks s1 to 2.
Pętla while wydrukuje wszystkie znaki i dodatkowo powiększy ostatni indeks i + 1
co spodowuje, że i = 3. Spowoduje to wyjście poza zakres.
Aby tego uniknąć należy w pętli while wprowadzić ograniczenie
dla i poprzez warunek i < len(s1).
'''

s1 = 'xyz'  # długość ciągu s1 = 3, najwyższy indeks s1 to 2

i = 0

while i < len(s1) and not (s1[i] in 'aoeiouAOEIOU'):
    print(s1[i])
    i = i + 1

print(i)

print("\n*******************\n")

